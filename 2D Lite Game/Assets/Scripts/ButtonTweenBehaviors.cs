﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonTweenBehaviors : MonoBehaviour
{
    public void TweenExpandSize()
    {
        transform.DOScaleX(1.5f,0.5f);
        transform.DOScaleY(1.5f, 0.5f);
    }
    public void TweenResetSize()
    {
        transform.DOScaleX(1f, 0.5f);
        transform.DOScaleY(1f, 0.5f);
    }
    public void TweenShakeOnce()
    {
        transform.DOShakeScale(1f);
    }
}
