﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behavior/MoveToPosition")]
public class MoveToPositionBehavior : FlockBehavior
{

    public Vector3 Pos;
    public Transform m_transform;

    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        Vector3 distance = Pos - agent.transform.position;

        if (distance.magnitude <= 0.03f)
        {
            return Vector2.zero;
        }
        Vector2 distance2d = new Vector2(distance.x, distance.y);
        return distance/2f;

    }
}
