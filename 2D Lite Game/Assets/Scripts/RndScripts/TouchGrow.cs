﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TouchGrow : MonoBehaviour
{

    public float charge_value = 0f;
    public GameObject CastCircle;
    public TextMeshProUGUI m_Text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 )
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                //touch.position;
            }
            if (touch.phase == TouchPhase.Stationary)
            {
                charge_value += Time.deltaTime;
                CastCircle.transform.localScale = new Vector3(charge_value, charge_value, 1f);
            }
            if (touch.phase == TouchPhase.Ended)
            {
                Release();
            }
        }
        m_Text.text = charge_value.ToString();
    }

    private void Release()
    {
        Debug.Log("Released at " + charge_value);
        charge_value = 0f;
    }
}
