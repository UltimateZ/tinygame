﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenPositionTest : MonoBehaviour
{
    public Vector2 Offset;
    float delta_X;
    float delta_Y;
    Vector2 delta_V = new Vector2(0f,0f);
    RectTransform m_rectTransform;
    // Start is called before the first frame update
    void Start()
    {
        m_rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            
            //Debug.Log(GetComponent<RectTransform>().anchoredPosition);
            //Debug.Log(Camera.main.WorldToScreenPoint(transform.position));// Use this.
            //Debug.Log(Camera.main.WorldToScreenPoint(GetComponent<RectTransform>().anchoredPosition));// Don't Use this.
        }

        if (Input.GetKeyUp(KeyCode.B))
        {
            RayCastFromScreenPosition();
        }

        if (Input.GetKeyUp(KeyCode.C))
        {
            MoveToCenterPosition();
        }

        FollowMousePositionOnScreen();
    }

    private void MoveToCenterPosition()
    {
        delta_X = Camera.main.WorldToScreenPoint(transform.position).x - Camera.main.pixelWidth/2;
        delta_Y = Camera.main.WorldToScreenPoint(transform.position).y - Camera.main.pixelHeight/2;
        delta_V = new Vector2(delta_X, delta_Y);
        m_rectTransform.anchoredPosition -= delta_V;
    }
    void RayCastFromScreenPosition()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(transform.position));
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            Debug.Log(hit.transform.name);
        }
    }
    void FollowMousePositionOnScreen()
    {
        delta_X = Camera.main.WorldToScreenPoint(transform.position).x - Input.mousePosition.x;
        delta_Y = Camera.main.WorldToScreenPoint(transform.position).y - Input.mousePosition.y;
        delta_V = new Vector2(delta_X, delta_Y);
        m_rectTransform.anchoredPosition -= delta_V;
    }
}
