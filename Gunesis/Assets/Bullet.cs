﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementalDamage
    {
        LIGHT,
        FIRE,
        ICE,
        POISON,
        ELECTRIC
    }

public class Bullet : MonoBehaviour
{
    public float speed;
    public float damage;
    public EnemyStats m_CurrentEnemy;


    private bool LightEnabled = false;
    private bool FireEnabled = false;
    private bool IceEnabled = false;
    private bool PoisonEnabled = false;
    private bool ElectricityEnabled = false;
    private float LightDamage;
    private float FireDamage;
    private float IceDamage;
    private float PoisonDamage;
    private float ElectricityDamage;


    

    delegate void MultiDamageDelegate();
    MultiDamageDelegate m_ApplyMultipleDamage;
    
    // Start is called before the first frame update
    void Start()
    {
        m_ApplyMultipleDamage += DealBaseDamage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Move()
    {
        transform.Translate(transform.forward * speed);
    }
    public virtual void DealBaseDamage()
    {
        m_CurrentEnemy.AdjustDamage(damage);
    }

    public virtual void DealElementalDamage()
    {
        if (LightEnabled)
        {
            m_CurrentEnemy.AdjustDamage(LightDamage);
            CreateLightBullet();
        }
        if (FireEnabled)
        {
            m_CurrentEnemy.AdjustDamage(FireDamage);
            m_CurrentEnemy.AddDamageOverTime(0.3f * FireDamage);
        }
        if (IceEnabled)
        {
            m_CurrentEnemy.AdjustDamage(IceDamage);
            // ++++Apply Frozen Coroutine

        }
        if (PoisonEnabled)
        {
            m_CurrentEnemy.AdjustDamage(PoisonDamage);
            m_CurrentEnemy.AddDamageOverTime(0.3f * PoisonDamage);
        }
        if (ElectricityEnabled)
        {
            m_CurrentEnemy.AdjustDamage(ElectricityDamage);
            // ++++Apply Nearby Enemy Damage
            ElectrifyNearbyEnemy();
        }

    }

    private void ElectrifyNearbyEnemy()
    {
        // Electrify all enemies within a certain radius
    }

    private void CreateLightBullet()
    {
        // Create 2 smaller light bullet at the point of contact
    }

    private void PerformWallHitEffect()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<EnemyStats>())
        {
            m_CurrentEnemy = other.GetComponent<EnemyStats>();
            m_ApplyMultipleDamage();
        }
        if (other.tag == "wall")
        {
            PerformWallHitEffect();
        }
    }
}
