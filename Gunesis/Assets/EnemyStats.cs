﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    private float HP = 100f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (HP <= 0f)
        {
            Death();
        }
    }
    private void Death()
    {
        // Use Object Pooling to RECYCLE later.
        Destroy(this.gameObject);
    }

    public void AdjustDamage(float bulletDamage)
    
    {
        HP -= bulletDamage;
        if (HP <= 0f)
        {
            Death();
        }
    }

    public void AddDamageOverTime(float n)
    {
        StartCoroutine(DamageOverTime(n));
    }

    IEnumerator DamageOverTime(float n)
    {
        for (int i = 0; i < 3f; i++)
        {
            yield return new WaitForSeconds(1f);
            AdjustDamage(n);
        }
    }

}
